#!/usr/bin/env bash
set -e
source "./venv/bin/activate"

# python file.py argv1 argv2 argv3 argv4
#
# argv1: caminho/nome completo do arquivo .log: /var/log/arq.log
# argv2: Intervalo de tempo de monitoramento do arquivo (em segundos): 1 segundo
# argv3: Sentença a ser monitorada: 'giropop gota'
# argv4: Comando a ser executado quando encontrar a sentença: 'systemctl restart time'
#
# Run test
# ./run.sh /root/.pm2/logs/siensws-out.log 0 'Chave Transacao inválida para este beneficiario' 'pwd'
python3 -u executa_comando_quando_encontra_sentenca.py $1 "$2" "$3" "$4"

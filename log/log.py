﻿# -*- coding: utf-8 -*

import logging


class Head:
    """"""

    def __init__(self):
        self.__path = './log/files/'
        self.__extension = 'log'
        self.__nameToFile = 'script'

    def run(self):
        """"""
        from time import strftime
        from logging import basicConfig
        basicConfig(filename='{}{}_{}.{}'.format(self.__path,
                                                 strftime('%Y%m%d'),
                                                 self.__nameToFile,
                                                 self.__extension
                                                 ),
                    filemode='a',
                    # force=True,
                    format='%(asctime)s; %(name)s; %(levelname)s; %(message)s',
                    level=logging.INFO
                    )


# Database
class Program:

    @staticmethod
    def startProgram():
        Head().run()
        text = 'Programa; Sucesso; Start'
        logging.info(text)
        return text

    @staticmethod
    def closeProgram(text):
        Head().run()
        text = 'Programa; Sucesso; Close - {}'.format(text)
        logging.info(text)
        return text

    @staticmethod
    def openFile(text):
        Head().run()
        text = 'Programa; Error; Open file log - {}'.format(text)
        logging.error(text)
        return text


class Monitoring:

    @staticmethod
    def persistSucess(text=''):
        Head().run()
        text = 'Monitorando; Sucesso; Persistência de dados - {}'.format(text)
        logging.info(text)
        return text

    @staticmethod
    def persistError(text=''):
        Head().run()
        text = 'Monitorando; Erro; Persistência de dados - {}'.format(text)
        logging.error(text)
        return text
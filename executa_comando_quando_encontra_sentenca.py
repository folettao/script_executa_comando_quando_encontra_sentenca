import shlex
import subprocess
import sys
from os import system
from time import sleep

from log import log

""" python file.py argv1 argv2 argv3 argv4

argv1: caminho/nome completo do arquivo .log: /var/log/arq.log
argv2: Intervalo de tempo de monitoramento do arquivo (em segundos): 1 segundo
argv3: Sentenca a ser monitorada: "giropop gota"
argv4: Comando a ser executado quando encontrar a sentenca: "systemctl restart time"
"""


class Programa:

    def __init__(self):
        self.beginLine = int(0)  # Default begin in line 0

    def __isNumber(self, number: str):
        try:
            float(number)
        except ValueError:
            try:
                complex(number)
            except ValueError:
                return False
        return True

    def __setLineBegin(self, path: str):
        try:
            return int(
                subprocess.check_output(
                    shlex.split('wc -l {}'.format(path))
                ).decode('utf-8').strip().split(" ")[0]
            ) + 1
        except Exception:
            self.__validFileExist(path)
            return self.__setLineBegin(path)

    def __openFile(self, path: str, operation='r'):
        try:
            return open(path, operation).read().splitlines()[self.beginLine:]
        except IOError as ex:
            log.Program.openFile(ex)
            self.__validFileExist(path)
            return self.__openFile(path)

    def __validFileExist(self, path: str):
        while system('ls {}'.format(path)) == 512:
            sleep(5)  # Wait 5 seconds to run again

    def main(self, argv: list):
        if len(argv) != 5:
            raise ValueError('Quantidade de argumentos inválida (leia as instruções no cabeçalho do programa)')
        else:
            if not self.__isNumber(argv[2]):
                raise ValueError('Valor passado para intervalo de tempo inválido')

        while True:
            if self.__setLineBegin(argv[1]) != self.beginLine:
                file = self.__openFile(argv[1])

                if file is not False:
                    for line in file:
                        if argv[3] in line:
                            system(argv[4])
                            log.Monitoring.persistSucess(line)
                    self.beginLine = self.__setLineBegin(argv[1])

            sleep(float(argv[2]))


if __name__ == '__main__':
    error = str()

    try:
        log.Program.startProgram()
        Programa().main(sys.argv)
    except ValueError as ex:
        error = ex
        print(error)
        log.Program.closeProgram(ex)
    except Exception as ex:
        error = 'Erro inesperado: {}'.format(ex)
        print(error)
    except KeyboardInterrupt as ex:
        error = 'Interrupcao via console (ctrl + c)'
    finally:
        log.Program.closeProgram(error)
